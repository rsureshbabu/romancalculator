
#include<stdio.h>
#include<string.h>
#include<stdbool.h>
#include "RomanMath.h"

/*******************************   Constants ************************************/
#define PASS 1
#define FAIL 0
/*******************************   Function Prototypes **************************/
static unsigned int convToDecimal(char *romanNumber);
static void convertToRoman (unsigned int val,char *romanNumber );
static int digitDecValue(char c);
static bool parseInput(char * romanNumber, char romanParam[][100],int * count1,int * count2);


/*******************************   Function Definitions **************************/
void romanCalc(char * romanMath,char * romanResult)
{
    /**************************   Local Variables ******************************/
    int count1=0,count2=0;
    unsigned int number =0,number1=0,number2=0;
    char romanParam[2][100]={{'\0'}};
    char flag;
    /*******************************   Code ************************************/
    
    // Parse/validate the given input roman math expression. Extract the operands.
    if(strlen(romanMath)>200)
    {
	printf("\nInvalid input\n");
	return ;
    }
    flag = parseInput(romanMath,romanParam,&count1,&count2);
    if(PASS == flag)
    {
        // Convert the roman operands to decimal
	number1 = convToDecimal(romanParam[0]);
	number2 = convToDecimal(romanParam[1]);

	if(number1==0 || number2==0)
	{
		return ;
	}
	else
	{
                // Perform the required math
		if(count1)
		{
		   number = number1 + number2;
		   if(((long unsigned int)(number1 + number2))>65536)
		   {
		   	printf("\nInvalid ! Sum of Inputs exceed the max limit 65536  \n");
			return ;
		   }
                   // Convert the result back to roman
		   convertToRoman (number,romanResult);
		}
		else
		{
			if(number1 > number2)
			{
				number=number1-number2;
				convertToRoman (number,romanResult);
			}
			else
			{
				printf("\nInvalid ! Number1 is less than or equal to Number2  \n");
				return ;
			}
		}			
	}
    }
    return ;
}

static unsigned int convToDecimal(char *romanNumber)
{
    /**************************   Local Variables ******************************/
     unsigned int number =0;
     int i=0;
    /*******************************   Code ************************************/
    // Take each character of the roman digit and process it
    while(romanNumber[i])
    {
        // Check if valid Roman Digit
        if(digitDecValue(romanNumber[i]) < 0)
	{
             printf("\nInvalid roman digit : %c",romanNumber[i]);
             return 0;
        }
        // Check the roman number rules    
        if((strlen(romanNumber) -i) > 2)
	{
             if(digitDecValue(romanNumber[i]) < digitDecValue(romanNumber[i+2]))
	     {
                printf("\nInvalid roman number %s \n",romanNumber);
                return 0;
             }
        }	 
	if((strlen(romanNumber) -i) > 1)
	{
		if((digitDecValue(romanNumber[i]) == digitDecValue(romanNumber[i+1])) && 
		    ((digitDecValue(romanNumber[i])==5) || (digitDecValue(romanNumber[i])==50) || (digitDecValue(romanNumber[i])==500)))
		{
			printf("\nInvalid roman number %s \n",romanNumber);
			return 0;
		}
	}
	if((strlen(romanNumber) -i) > 3)
	{
		if((digitDecValue(romanNumber[i]) == digitDecValue(romanNumber[i+1])) && (digitDecValue(romanNumber[i]) == digitDecValue(romanNumber[i+2])) &&
		    (digitDecValue(romanNumber[i]) == digitDecValue(romanNumber[i+3])) &&
		    ((digitDecValue(romanNumber[i])==1) || (digitDecValue(romanNumber[i])==10) || (digitDecValue(romanNumber[i])==100)))
		{
			printf("\nInvalid roman number %s \n",romanNumber);
			return 0;
		}
	}
        // If the above checks looks valid, compute the decimal equivalent
        if(digitDecValue(romanNumber[i]) >= digitDecValue(romanNumber[i+1]))
             number = number + digitDecValue(romanNumber[i]);
        else
		{
            number = number + (digitDecValue(romanNumber[i+1]) - digitDecValue(romanNumber[i]));
            i++;
        }
        i++;
    }
    return number;
}

static void convertToRoman (unsigned int val,char *romanNumber )
{
    /**************************   Local Variables ******************************/
    char *huns[] = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    char *tens[] = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    char *ones[] = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
    int   size[] = { 0,   1,    2,     3,    2,   1,    2,     3,      4,    2};

    /*******************************   Code ************************************/
    while (val >= 1000) {
        *romanNumber++ = 'M';
        val -= 1000;
    }
    // Compute the roman equivalent using LookUpTable
    strcpy (romanNumber, huns[val/100]); romanNumber += size[val/100]; val = val % 100;
    strcpy (romanNumber, tens[val/10]);  romanNumber += size[val/10];  val = val % 10;
    strcpy (romanNumber, ones[val]);     romanNumber += size[val];

    *romanNumber = '\0';
	
    return ;
}
static int digitDecValue(char c)
{
    /**************************   Local Variables ******************************/
     int value=0;
    /*******************************   Code ************************************/
    switch(c)
    {
         case 'I': value = 1; break;
         case 'V': value = 5; break;
         case 'X': value = 10; break;
         case 'L': value = 50; break;
         case 'C': value = 100; break;
         case 'D': value = 500; break;
         case 'M': value = 1000; break;
         case '\0': value = 0; break;
         default: value = -1; 
    }

    return value;
}

static bool parseInput(char * romanNumber, char romanParam[][100],int * count1,int * count2)
{
    /**************************   Local Variables ******************************/
	int i=0,j=0;
	char * pch;
    /*******************************   Code ************************************/
        // Compute the number of addition/subtraction in the given math expression
	for (i=0; romanNumber[i]!='\0';i++)
	{
		if(romanNumber[i]=='+')
		{
	  		*count1 = *count1 + 1;
		}
	    if(romanNumber[i]=='-')
		{
	  		*count2 = *count2 + 1;
		}
	}
        // Expect only one addition/subtraction in the given math expression
	if(*count1>1 || *count2>1 || (*count1+*count2)>1 )
	{
        	printf("\nInvalid ! Only one Addition/Subtraction can be perfomed at a time  \n");
		return(FAIL);
	}
	if(*count1==0 && *count2==0)
	{
       		printf("\nInvalid arithmetic operations specified  \n");
		return(FAIL);
	}
	// Extract the two operands from the given math expression
	pch = strtok(romanNumber,"+-");
	i=0;
	while (pch != NULL)
	{
		j=0;
		while(*pch!='\0')
		{
			romanParam[i][j]=*pch++;
			j++;
		}
		i++;
		pch = strtok (NULL, "+-");
	}
	// if the number of operands is less than two,input is invalid
	if(i<2)
	{
		printf("\nInvalid input string \n");
		return(FAIL);
	}
	
	return(PASS);	
}
