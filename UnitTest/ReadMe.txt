The Unit test includes the following set of scenarios:

1) Validate the basic addition/subtraction scenarios

	
2) Validate the roman digit rules: If any rule violation, make sure function returns NULL
	
3) Validate the allowed operations: If any rule violation, make sure function returns NULL
	

4) Validate the allowed number ranges: If any rule violation, make sure function returns NULL
