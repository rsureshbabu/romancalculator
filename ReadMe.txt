
This folder contains Source code, Library, and Unit test for roman Calculator.

API to use to invoke library function:

void romanCalc(char * romanMath,char * romanResult);

Input arguments : 
   char * romanMath   --- Pointer to Input character buffer of size atleat 200 bytes containing roman math expression
   char * romanResult --- Pointer to Output character buffer of size atleast 100 bytes containing roman math result

Return Values :   
   None
